# En este script vamos a ver como trabajar
# con algoritmos gen�ticos.

# Hasta ahora, trabajamos con problemas lineales resolvien-
# dolos mediante el m�todo simplex, y con problemas no 
# lineales utilizando tres m�todos diferentes con la 
# funci�n optim (Brent, Nelder-Mead y BFGS). Vamos a 
# estudiar ahora un conjunto de m�todos mas robustos
# englobados dentro de la familia de los algoritmos
# evolutivos, los algoritmos gen�ticos.

# El paquete a utilizar es el "genalg", entonces hacemos:

install.packages("genalg")
library("genalg")

# Parte del poder de los algoritmos gen�ticos es que
# su operaci�n abstrae cualquier detalle de la estructura
# del problema, reduciendose todas las variables de 
# decisi�n a un "cromosoma". La forma estandar de 
# trabajo es mediante cromosomas con "genes" binarios.
# Asi que empecemos con este tipo de problemas. 

# Supongamos el siguiente problema de minimizaci�n:

# Max Z = XA * 24 + XB * 20
# Sujeto a:
#   10 * XA + 7 * XB <= 200 
#   1 * XA + 0.5 * XB <= 12
#   1 * XA + 0 * XB <= 15
#   0 * XA + 1 * XB <= 24
#   1 * XA + 1 * XB <= 20
#
# Con todas las variables no negativas y donde
# el problema consiste en maximizar la cantidad
# de vapor generada por tonelada de carb�n (X) 
# en una caldera, siendo la primer restricci�n
# el presupuesto que se dispone para gastar en
# carb�n, la segunda la cantidad m�xima de kg
# de humo por tonelada de carb�n quemado 
# que se puede generar, la segunda y la tercera
# las capacidades de proceso para los carbones 
# A y B, y la �ltima la capacidad de 
# transporte de carb�n.

# A fin de utilizar el paquete "genalg", es necesario
# convertir el problema anterior al formalismo de los
# algoritmos gen�ticos. Lo primero es decidir como 
# codificamos las variables. A priori, dado que tenemos
# dos variables, nuestros cromosomas deber�an tener
# dos genes solamente:

# CROMOSOMA = GEN_A | GEN_B

# El siguiente paso es definir con cuantos bits 
# codificamos cada gen. En este caso, tenemos
# dos restricciones que podemos usar como
# cotas naturales:

#   1 * XA + 0 * XB <= 15  - Cota XA
#   0 * XA + 1 * XB <= 24  - Cota XB

# XA debe pertenecer al intervalo [0 ; 15]:

# 0000 = 0
# ...
# 1111 = 15

# Haciendo el mismo an�lisis con XB, podemos
# ver que la potencia de 2 inmediatamente
# superior a 24 es 32 = 2^5, por lo cual:

# 00000 = 0
# ...
# 11111 = 31

# Si lo pensamos un poco, trabajar en binario 
# puede ser un poco tedioso, as� que hagamos
# dos funciones para hacer las conversiones:

pasarABaseBinariaDesdeDecimal <- function(numero){
  numero.redondeado <- floor(numero)
  binario <- c()
  while (numero.redondeado>=2){
    binario <- c(binario, numero.redondeado %% 2)
    numero.redondeado <- numero.redondeado %/% 2
  }
  binario <- c(binario, numero.redondeado)
  binario[length(binario):1]
}

pasarABaseDecimalDesdeBinaria <- function(arrayDeBits){
  cantidadBits <- length(arrayDeBits)
  multiplicador <- rep(2, times=cantidadBits)
  multiplicador <- multiplicador^(order(multiplicador)-1) 
  multiplicador <- multiplicador[order(multiplicador, decreasing = TRUE)]
  sum(arrayDeBits * multiplicador)
}

# Y las pruebo:
pasarABaseBinariaDesdeDecimal(125)
pasarABaseDecimalDesdeBinaria(c(1,1,1,1,1,0,1))

# Bueno, ya codificada nuestra soluci�n, tenemos
# que analizar como armar nuestra funci�n objetivo. 
# teniendo en cuenta el efecto de las restricciones:
# Una forma bastante sencilla
# puede ser que la funci�n objetivo sea
# XA * 24 + XB * 20 si no se viola ninguna
# restricci�n, y si se violan, ya que es un problema
# de maximizaci�n, reducir el valor objetivo en
# M * (valor absoluto del desv�o), siendo M
# un n�mero muy grande. Entonces, podemos
# escribir nuestra funci�n objetivo como:

funobj <- function(XA, XB){
  # Defino el valor de M
  M <- 50
  
  # Chequeo las restricciones:
  desvio <- abs(min(0,200-(10 * XA + 7 * XB)))
  desvio <- desvio + abs(min(0,12-(1 * XA + 0.5 * XB)))
  desvio <- desvio + abs(min(0,16-(1 * XA + 0 * XB)))  # No necesaria, ya est� acotada
  desvio <- desvio + abs(min(0,24-(0 * XA + 1 * XB)))  # Es necesaria, la cota calculada es 32
  desvio <- desvio + abs(min(0,20-(1 * XA + 1 * XB)))
  
  XA * 24 + XB * 20 - M * desvio
}

# Pero recordemos que tenemos que trabajar con 
# cadenas de bits, as� que hagamos una funci�n 
# mas binaria:

funobjbin <- function(cromosoma){
  cromosoma.XA <- cromosoma[1:4]
  cromosoma.XB <- cromosoma[5:9]
  
  XA <- pasarABaseDecimalDesdeBinaria(cromosoma.XA)
  XB <- pasarABaseDecimalDesdeBinaria(cromosoma.XB)  
  
  funobj(XA, XB)
}

# Codificadas nuestras variables de decisi�n y
# armada la funci�n objetivo, ya podemos usar el
# paquete "genalg". La funcion a utilizar es
# es rbga.bin(), la cual minimiza, por lo
# tanto tenemos que invertir el signo de nuestra
# funci�n objetivo. Es este caso, mediante
# una funci�n an�nima:

myProb <- rbga.bin(size=9, evalFunc = function(x){-1*funobjbin(x)}, 
                   showSettings = TRUE)
myProb

# Donde "size" es el tama�o de cada cromosoma.
# Usamos el "showSetting = TRUE" para mostrar la
# configuraci�n por defecto que utilizamos. Analicemos
# un poco el objeto que nos devuelve la funci�n:

myProb$popSize

# El algoritmo funcion� con un tama�o de poblaci�n
# igual a 200, o sea, en cada iteraci�n se evaluaron
# 200 potenciales soluciones del problema (en t�rminos de
# Algoritmos Gen�ticos, cromosomas). Si vemos las soluciones:

myProb$population

# Tenemos una matriz de 200 filas (una por cada cromosoma en
# la poblaci�n con la mejor soluci�n alcanzada) y 10
# columnas (el tama�o de cada cromosoma que la funci�n
# supone que estamos utilizando).
# Para obtener nuestra mejor soluci�n:

myProb$evaluations

# Y tenemos los valores de funci�n objetivo para
# cada uno de los 200 cromosomas en la poblaci�n
# final. Nosotros nos tenemos que quedar con
# la soluci�n con el mejor valor de funci�n
# objetivo, entonces:

order(myProb$evaluations)

myProb.solucion <- myProb$population[order(myProb$evaluations)[1],]
myProb.solucion

# Si quiero saber a que valores de mis variables
# objetivos corresponden, las decodifico:

myProb.XA <- pasarABaseDecimalDesdeBinaria(myProb.solucion[1:4])
myProb.XB <- pasarABaseDecimalDesdeBinaria(myProb.solucion[5:9])

# Y ya que estamos:

myProb.obj <- -1 * min(myProb$evaluations)

# Entonces:

myProb.XA
myProb.XB
myProb.obj

# rbga.bin() trabaj� con casi todos los par�metros por
# defecto, pero podemos modificarlos antes de hacer otra
# corrida:

myProb <- rbga.bin(size=9, popSize = 100, iters = 500,
                   mutationChance = 0.05, elitism = 10, 
                   evalFunc = function(x){-1*funobjbin(x)}, 
                   showSettings = TRUE)
myProb

myProb.solucion <- myProb$population[order(myProb$evaluations)[1],]
myProb.XA <- pasarABaseDecimalDesdeBinaria(myProb.solucion[1:4])
myProb.XB <- pasarABaseDecimalDesdeBinaria(myProb.solucion[5:9])
myProb.obj <- -1 * min(myProb$evaluations)

myProb.XA
myProb.XB
myProb.obj

# La funci�n rbga.bin() es muy interesante para
# analizar como evoluciona la poblaci�n en cada iteraci�n.
# Analizando el objeto que devuelve, tenemos los
# atributos mean y best, que nos muestran para cada
# iteraci�n el promedio del valor objetivo y el mejor
# valor objetivo respecto de la poblaci�n vigente en 
# ese momento:

myProb$mean
myProb$best

# Podemos ver la evoluci�n de las soluciones en
# un gr�fico:

plot(myProb)


# El paquete "genalg" es bastante �til para la
# aplicaci�n pr�ctica de los Algoritmos Gen�ticos, 
# pero quizas nos interese armar nuestra propia
# biblioteca de funciones para Algotimos Gen�ticos,
# de manera de probar otras configuraciones o nuevas
# t�cnicas. As� que armemos una funci�n b�sica
# de optimizaci�n basada en este paradigma para tener
# de ejemplo.
# Primero que todo, armemos una funci�n que nos
# genere una poblaci�n inicial aleatoria:

generarPoblacionInicial <- function(tamanoCromosoma, tamanoPoblacion){
  
  matrix(round(runif(tamanoCromosoma * tamanoPoblacion)), 
                             nrow = tamanoPoblacion, 
                             ncol = tamanoCromosoma, 
                             byrow = TRUE)

}

# Vamos a probarla :

generarPoblacionInicial(5,10)

# Hagamos una funci�n para seleccionar las n mejores:

seleccionarMejores <- function (poblacion, funcionObjetivo, cantMejores, minimizar = TRUE){
  valoresObj <- c()
  for (i in 1:nrow(poblacion)){
    valoresObj <- c(valoresObj, funcionObjetivo(poblacion[i,1:ncol(poblacion)]))
  }
  order(valoresObj, decreasing = !minimizar)[1:cantMejores]
}

# Prob�mosla:

datos <- generarPoblacionInicial(5,10)
datos
seleccionarMejores(datos, function(x){sum(x)}, cantMejores = 3, minimizar = FALSE)

# Ya sabemos como generar la poblaci�n inicial
# y como seleccionar por las mejores. Veamos como mutar 
# a poblacion:

mutarCromosomas <- function(poblacion, porcentajeMutacion){
  cantCromosomas <- nrow(poblacion)
  cromosomasMutados <- ceiling(poblacion * porcentajeMutacion)
  cromosomasSeleccionados <- sample(1:cantCromosomas,cromosomasMutados,replace=FALSE)
  bitsSeleccionados <- sample(1:ncol(poblacion),cromosomasMutados,replace=TRUE) 
  
  nuevaPoblacion <- poblacion
  nuevaPoblacion[cromosomasMutados,bitsSeleccionados] <- 
    ((poblacion[cromosomasMutados,bitsSeleccionados]+1) %% 2)
  
  nuevaPoblacion
}

# Probemos nuestra funci�n de mutaci�n

datos
mutarCromosomas(datos, 0.25)

# Y ahora armemos la funci�n para generar hijos:

generarHijosDeMadreYPadre <- function(madre, padre, cantHerenciaMadre){
  hijo <- padre
  herenciaMadre <- sample(1:length(madre), cantHerenciaMadre, replace = FALSE)
  hijo[herenciaMadre] <- madre[herenciaMadre]
  hijo
  
}

# La probamos:

generarHijosDeMadreYPadre(c(1,1,1,1,1), c(0,0,0,0,0), 3)
generarHijosDeMadreYPadre(c(1,1,1,1,1), c(0,0,0,0,0), 3)
generarHijosDeMadreYPadre(c(1,1,1,1,1), c(0,0,0,0,0), 3)

# Armemos ahora la funci�n que implementa el algoritmo
# en forma completa:

myAlgoritmoGenetico <- function(tamanoCromosoma, tamanoPoblacion,cantMejores,porcentajeMutacion,cantHerenciaMadre, nroIteraciones = 100, funcionObjetivo, minimizar = TRUE){
  
  poblacion <- generarPoblacionInicial(tamanoCromosoma, tamanoPoblacion)
  mejoresCromosomas <- c()
  
  for (i in 1:nroIteraciones){
    mejoresCromosomas <- seleccionarMejores(poblacion,funcionObjetivo, cantMejores, minimizar)
    nuevaPoblacion <- poblacion[mejoresCromosomas,]
    for (j in 1:(nrow(poblacion) - length(mejoresCromosomas))){
      padres <- sample(mejoresCromosomas, 2, replace = FALSE)
      nuevaPoblacion <- rbind(nuevaPoblacion, generarHijosDeMadreYPadre(poblacion[padres[1],],poblacion[padres[2],],cantHerenciaMadre))
    }
    poblacion <- mutarCromosomas(nuevaPoblacion, porcentajeMutacion)    
  }
  poblacion
}

# La probamos:

myAlgoritmoGenetico(tamanoCromosoma=10, tamanoPoblacion=100,cantMejores=10,porcentajeMutacion=0.1,cantHerenciaMadre=5, nroIteraciones = 100, funcionObjetivo=function(x){sum(x)}, minimizar = FALSE)
myAlgoritmoGenetico(tamanoCromosoma=10, tamanoPoblacion=100,cantMejores=10,porcentajeMutacion=0.1,cantHerenciaMadre=5, nroIteraciones = 100, funcionObjetivo=function(x){sum(x)}, minimizar = TRUE)