# Bueno, vamos a comenzar nuestro taller de 
# aplicaciones de optimizaci�n con R. El
# objetivo del mismo es poder demostrar la 
# aplicaci�n de esta herramientas tanto a 
# la resoluci�n de problemas de optimizaci�n
# como a la ense�anza de los mismos.

# Antes de empezar con programaci�n lineal,
# empecemos a tratar el tema de la optimizaci�n
# con un enfoque un poco mas general. En mis 
# clases me gusta (y me resulta mas pr�ctico)
# comenzar con la interpretaci�n geom�trica
# de un problema de optimizaci�n, en vez de
# iniciar el tema mediante la resoluci�n de
# sistemas de ecuaciones. Entonces bueno,
# abramos el R y comencemos a trabajar un
# poco.

# Vamos a comenzar con un problema f�cil,
# para familiarizarnos con la metodolog�a
# de trabajo de R. Pensemos en el problema
# de encontrar el m�nimo de una par�bola.
# Primero lo primero, �como representamos una
# par�bola en R?. La forma mas sencilla
# es mediante una funci�n propia:

parabola <- function(x){
  x*x
}

# Con la secuencia anterior, creamos una 
# funci�n llamada par�bola, que toma un solo
# par�metro (x) y devuelve el valor del 
# par�metro elevado al cuadrado. Por ejemplo:

parabola(5)
parabola(-10)

# Como R trabaja vectorizando siempre que se
# pueda, lo siguiente es v�lido:

valoresX <- c(-5,-4,-3,-2,-1,0,1,2,3,4,5)
parabola(valoresX)

# Como vimos, podemos crear una variable 
# vectorial llamada "valoresX" con
# la funci�n "c()" y pasarla como par�metro
# a nuestra funci�n, y la misma eleva
# al cuadrado a todas las componentes del vector,
# �automaticamente!

# Sabiendo esto, armemos una peque�a gr�fica
# para poder visualizarla:

valoresX <- c(-100:100)
valoresY <- parabola(valoresX)
plot(valoresX, valoresY, type="l", 
     main="Grafica de f(x) = x*x", 
     xlab= "Dominio", ylab="F(x)=x*x")

# Bueno, pero este era un taller de
# optimizaci�n, no de gr�ficos. As� que
# armemos nuestro primer algoritmo de 
# optimizaci�n, una b�squeda aleatoria.
# Pensemos un poco como deber�a trabajar. 
# Primero, nuestra funci�n de b�squeda
# aleatoria deber�a tener un funci�n 
# a optimizar, asi que previamente
# tenemos que definirla. En nuestro caso,
# ya tenemos a la par�bola. Una vez con la
# funci�n, deberiamos generar valores 
# aleatorios para x y chequear el valor de F(x)
# asociado. Pero no podemos generar valores 
# aleatorios distribuidos entre todos los reales,
# deber�amos acotar nuestra b�squeda. Entonces,
# nuestro optimizador deber�a recibir como
# par�metro la cota m�nima y la cota m�xima
# a partir de la cual implementar la b�squeda. 
# Ya tenemos dos par�metros mas. �Como genero
# n�meros aleatorio?, bueno, si son continuos,
# con alguna funci�n del tipo rDist, en nuestro 
# caso "runif":

valoresAleatorios <- runif(n=100, min=-1000, max=1000)
View(valoresAleatorios)

# �Y como sabemos cual valor de un vector es el
# m�nimo?. Usamos "order":

indice <- order(valoresAleatorios, decreasing = FALSE)
indiceMenor <- indice[1]
valoresAleatorios[indiceMenor]

# Sabiendo esto, podemos armar la funci�n del
# optimizador:

busquedaAleatoria <- function(fn, cotaMin, cotaMax){
  valoresX <- runif(100, cotaMin, cotaMax)
  valoresY <- fn(valoresX)
  
  indice <- order(valoresY, decreasing = FALSE)
  indiceMenor <- indice[1]
  resultado <- c(valoresX[indiceMenor],valoresY[indiceMenor])
  resultado
}

busquedaAleatoria(parabola, -100, 100)
# Ya tenemos una funci�n que nos optimiza
# por b�squeda aleatoria. Pero muy pobre, 
# solo minimiza, y no me deja elegir que tantas
# pruebas quiero hacer. La podemos retocar un poco:

busquedaAleatoria2 <- function(fn, cotaMin, cotaMax, 
                              maximizar=FALSE,cantMuestras=100){
  valoresX <- runif(cantMuestras, cotaMin, cotaMax)
  valoresY <- fn(valoresX)
  
  indice <- order(valoresY, decreasing = maximizar)
  indiceMenor <- indice[1]
  resultado <- c(valoresX[indiceMenor],valoresY[indiceMenor])
  resultado
}

busquedaAleatoria2(parabola, -100, 100, cantMuestras=10000)

# Ahora, a efectos de ense�anza, ser�a muy 
# interesante si la funci�n nos devuelve el
# hist�rico de valores generados, as� como
# la performance en tiempo:

busquedaAleatoria3 <- function(fn, cotaMin, cotaMax, 
                               maximizar=FALSE,cantMuestras=100,
                               devolverPerformance = FALSE){
  valoresX <- runif(cantMuestras, cotaMin, cotaMax)
  valoresY <- fn(valoresX)
  
  indice <- order(valoresY, decreasing = maximizar)
  indiceMenor <- indice[1]
  resultado <- c(valoresX[indiceMenor],valoresY[indiceMenor])
  
  if (devolverPerformance){
    salida <- list(resultado, cbind(valoresX, valoresY), 
                   maximizar, cantMuestras, indiceMenor)
    names(salida) <- c("resultado", "historico", 
                       "maximizar", "cantMuestras", 
                       "iteracionDelOptimo")
  }
  else{
    salida <- resultado
  }
  salida
}

# Entonces puedo hacer:

opt <- busquedaAleatoria3(parabola,-100,100, devolverPerformance=TRUE)
str(opt)

# Exploremos nuestra soluci�n:

opt$resultado
View(opt$historico)
opt$maximizar
opt$cantMuestras

# Grafic�ndolo:

plot(opt$historico[,1], opt$historico[,2])
plot(seq(1:opt$cantMuestras), opt$historico[,2], type="l")

# Podemos ver que no se sigue ning�n 
# criterio "inteligente" a la hora de generar
# las nuevas soluciones a evaluar.
# Es mas, podemos hacer:

points(opt$iteracionDelOptimo, opt$resultado[2], col="blue", pch=16)


# Esto de trabajar con una sola variable es bueno
# para graficar, pero deber�amos tener un m�todo
# estandar para optimizar para "n" variable. La
# transformaci�n es f�cil:

busquedaAleatoriaMultivariable <- function(fn, cotaMin, cotaMax, 
                               maximizar=FALSE,cantMuestras=100,
                               devolverPerformance = FALSE){
  
  valoresX <- runif(cantMuestras, cotaMin[1], cotaMax[1])
  
  if (length(cotaMin)>1){
    for (i in 2:length(cotaMin)){
      valoresX <- cbind(valoresX, runif(cantMuestras, cotaMin[i], cotaMax[i]))           
    }
  }
    
  valoresY <- fn(valoresX)
  
  indice <- order(valoresY, decreasing = maximizar)
  indiceMenor <- indice[1]
  resultado <- c(valoresX[indiceMenor],valoresY[indiceMenor])
  
  if (devolverPerformance){
    salida <- list(resultado, cbind(valoresX, valoresY), 
                   maximizar, cantMuestras, indiceMenor)
    names(salida) <- c("resultado", "historico", 
                       "maximizar", "cantMuestras", 
                       "iteracionDelOptimo")
  }
  else{
    salida <- resultado
  }
  salida
}

