# Vamos a ver ahora como hacer para evaluar que
# tan robusta es la soluci�n encontrada para
# nuestro problema de programaci�n lineal.
# Supongamos que tenemos este problema:
#
# Max Z = 5X1 + 9X2 + 3X3
# Sujeto a:
#   1X1 + 2X2 + 3X3 >= 1
#   3X1 + 2X2 + 2X3 <= 15
#   X1, X2, X3 >= 0
#
# Con lpSolve:

library("lpSolve")

# Limpiemos un poco la memoria de trabajo:

rm(list=ls())

# Y parametricemos nuestro problema:

problem.coef.obj <- c(5, 9, 3)
problem.coef.restr <-  matrix (c(1, 2, 3, 3, 2, 2), 
                               nrow=2, byrow=TRUE)
problem.dir.restr <- c(">=", "<=")
problem.term.restr <- c(1, 15)

mylp <- lp ("max", problem.coef.obj, 
            problem.coef.restr, 
            problem.dir.restr, 
            problem.term.restr)

mylp$solution
mylp$objval

# Pero, como sabemos, los coeficientes, en gran
# parte de las aplicaciones de optimizaci�n, no
# son determin�sticos, sin embargo se aproximan
# mediante sus esperanzas. Ahora bien, �que tan
# sensible es nuestra soluci�n a cambios en 
# los par�metros?. Empecemos por la funci�n
# objetivo. Supongamos que sabemos la
# distribuci�n de probabilidades asociadas
# a cada uno de ellos:
#
#   c1 = uniforme(min = -10, max = 20)
#   c2 = normal(media = 9, desv. std = 3.5)
#   c3 = normal(media = 3, desv. std = 1.5)
#
# Generemos muestras de tama�o 100 para cada
# coeficiente:

c1 <- runif(100, -10, 20)
c2 <- rnorm(100, 9, 3.5)
c3 <- rnorm(100, 3, 1.5)

# Analic�moslos:

View(c1)
summary(c1)
summary(c2)
summary(c3)

# Ahora armemos una funci�n para evaluar
# las variaciones de los coeficientes. En
# realidad, mejor definamos una funci�n
# lineal gen�rica:

funcionLineal <- function(coef, var){
  sum(coef*var)
}

# Obviamente, la funci�n admite vectorizaci�n:

funcionLineal(problem.coef.obj, mylp$solution)

# Definamos ahora una funci�n para evaluar
# escenarios:

evaluarEscenarios <- function(
  muestrasCoef, var, leyFO){
  evaluaciones <- c()
  for (i in 1:nrow(muestrasCoef)){
    evaluaciones <- c(evaluaciones, 
                      leyFO(muestrasCoef[i,],
                            var))
  }
  evaluaciones  
}

escenarios <- cbind(c1, c2, c3)
head(escenarios)
evaluaciones <- evaluarEscenarios(escenarios, 
                                  mylp$solution, 
                                  funcionLineal)

# Entonces, puedo obtener estad�sticas de la FO:
summary(evaluaciones)
hist(evaluaciones, col="blue")

# �Cuantas evaluaciones arrojan un valor de FO
# menor a 80?:

evalMenoresA80 <- evaluaciones[which(evaluaciones<=80)]
evalMenoresA80

# Pero mas pr�ctico es hacer una funci�n para
# evaluar la probabilidad que tenemos de un 
# escenario determinado:

probarEscenarios <- function(evalEscenarios, 
                             valorCritico, 
                             sentido="<="){
  if (sentido == "<="){
    frecAbs <- evalEscenarios[which(evalEscenarios<=valorCritico)] 
  }
  else{
    frecAbs <- evalEscenarios[which(evalEscenarios>=valorCritico)]
  }
  frecRel <- length(frecAbs) / length(evalEscenarios)
  frecRel
}

probarEscenarios(evaluaciones, 80)
probarEscenarios(evaluaciones, 90, ">=")

# Y tambi�n puedo hacer:

probarEscenariosEntre <- function(evalEscenarios, 
                             valorCriticoInferior, 
                             valorCriticoSuperior){

  frecAbs <- evalEscenarios[which((evalEscenarios<=valorCriticoSuperior)& 
    (evalEscenarios>=valorCriticoInferior))]
  frecRel <- length(frecAbs) / length(evalEscenarios)
  frecRel
}

probarEscenariosEntre(evaluaciones, 0,50)
probarEscenariosEntre(evaluaciones, 70,150)
probarEscenariosEntre(evaluaciones, 50,70)

# Todo muy lindo, pero que pasa si nuestro 
# modelo es no lineal. Bueno, en las funciones
# "evaluarEscenarios()", "probarEscenarios()" 
# y "probarEscenariosEntre()" no definimos 
# en ning�n momento ninguna condici�n
# de linealidad, as� que basta con pasar la 
# funci�n de evaluaci�n de escenarios apropiada,
# en general, la funci�n objetivo que utilizamos
# en la optimizaci�n.


# Con la misma l�gica, podemos optimizar en varios
# escenarios de manera sencilla. Volvamos a nuestros
# antiguos escenarios:

head(escenarios)

# Pero antes armemos una funci�n de evaluaci�n de
# escenarios:

lpXEscenarios <- function(direction, escenarios_objetivos, 
                          const.mat, const.dir, const.rhs){
  resoluciones <- list()
  for (i in 1:nrow(escenarios_objetivos)){
    resoluciones <- c(resoluciones,
                      list(lp(direction,escenarios_objetivos[i,], 
                              const.mat, const.dir, const.rhs)
                           )
                      ) 
       
  }
  resoluciones
}

mylpXEscenarios <- lpXEscenarios ("max", escenarios, 
            problem.coef.restr, 
            problem.dir.restr, 
            problem.term.restr)

# Si quiero ver las soluciones generadas:

soluciones <- NULL
for (i in 1:100){soluciones <- cbind(soluciones,
                                     mylpXEscenarios[[i]]$solution)}
View(soluciones)

# Entonces tengo una lista con los valores que
# optimizan mi FO para cada escenario. Ser�a 
# interesante probar cada soluci�n en cada escenario,
# y elegir la soluci�n que tenga el mejor
# desempe�o para el peor escenario:

selecMaxDelPeor <- function(escenarios, optimosPorEscenarios,
                            FO){
  desempenoMinimo <-  NULL
  for (i in 1:length(optimosPorEscenarios)){
    desempeno <- evaluarEscenarios(escenarios,
                                   optimosPorEscenarios[[i]]$solution,
                                   FO)
    desempenoMinimo <- c(desempenoMinimo,desempeno[order(desempeno)[1]])
  }
  maxDesempenoMinimo <- order(desempenoMinimo, decreasing=TRUE)[1]
  resultado <- optimosPorEscenarios[[maxDesempenoMinimo]]
  resultado$solution
}

selecMaxDelPeor(escenarios, mylpXEscenarios, funcionLineal)

# Bueno, hasta ac� evaluamos como medir la
# sensibilidad de la funci�n objetivo. Pero, �que
# pasa con las restricciones?. A priori, podr�amos
# reaprovechar lo que ya tenemos hecho, agregando
# una comparativa con el termino independiente
# de manera de poder sensibilizar tanto a
# este �ltimo como a los coeficientes:

evaluarRestriccion <- function(
  muestrasCoef, var, leyRest, 
  sentido=">=", muestraTermIndep){
  
  evalLadoIzquierdo <- evaluarEscenarios(
    muestrasCoef, var, leyRest)
  if (sentido==">="){
    comparativas = (evalLadoIzquierdo >= muestraTermIndep)
  }
  else{
    comparativas = (evalLadoIzquierdo <= muestraTermIndep)
  }
  evaluacion <- cbind(evalLadoIzquierdo, sentido, 
                      muestraTermIndep, comparativas)
  names(evaluacion) <- c("ladoIzquierdo", "tipo",
                         "ladoDerecho", "seSatisface")
  evaluacion
}


evaluarRestriccion(cbind(runif(100,1,2), runif(100, 0,5), runif(100,2,5)), 
                   mylp$solution,
                   funcionLineal,
                   ">=",
                   runif(100,0,5))

probarEscenariosRest <-function(evaluacion, probarSatisfaccion=TRUE){
  frecRel <- length(evaluacion[which(evaluacion==TRUE)])/
    length(evaluacion)
  frecRel
}