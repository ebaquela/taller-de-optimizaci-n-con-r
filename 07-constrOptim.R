# Continuemos viendo como hacer optimizaci�n no lineal.
# Hasta ahora, entonces, estuvimos trabajando con 
# optim para optimizaci�n. Es una funci�n muy buena, 
# pero trabaja minimizando funciones no restringidas.
# En la pr�ctica de la IO, en general nos tocan problemas
# que se caracterizan por un conjunto de restricciones que
# limitan nuestro rango de soluciones. En el caso
# que queramos minimizar funciones no lineales sujetas a
# restricciones lineales, disponemos de la funci�n
# "constrOptim()". En l�neas generales, es muy parecida
# a "optim()" pero se le agregan par�metros relativos
# a las restricciones. Minimicemos un paraboloide
# el�ptico, sujeto a restricciones del tipo >=.
# Para ello, usemos la funci�n del 
# paraboloide el�ptico:


myParaboloideEliptico <- function(x,y,par_a,par_b,par_corrx,
                                  par_corry, par_termIndep){
  ((x+par_corrx)/par_a)^2 + ((y+par_corry)/par_b)^2 + par_termIndep
}

# Con la funci�n myPersp puedo visualizarlo:

myPersp <- function(x, y, Fn, reemplazoNA=NA, ...){
  z<- outer(x,y,Fn)
  z[is.na(z)]<-reemplazoNA
  persp(x,y,z,...)
}

myPersp(c(-100:100), c(-100:100), function(x,y){myParaboloideEliptico(x,y,1,1,0,0,0)})

# Ahora defino las restricciones 

matrizA <- matrix(c(1,0,0,1), nrow=2, byrow=TRUE)
vectorb <- c(5,10)

# Y optimizo:

constrOptim(c(100,100), function(x){myParaboloideEliptico(x[1],x[2],1,1,0,0,0)}, 
            grad=NULL, ui=matrizA,ci=vectorb)

# Al igual de lo que pasaba con optim(), debo 
# proporcionarle un valor inicial de b�squeda,
# que en este caso, adem�s, debe estar dentro 
# de la regi�n factible.

# El objeto que devuelve es similar al que devuelve
# es similar al que devuelve optim(), pero con los
# par�metros adicionales "outer.iteration" y
# "barrier.value", donde el primero son las
# llamadas que se hacen a optim() y el segundo
# el valor de la barrera.
